# blog-dot
blog.bi0s.in proudly published by [teambi0s](https://bi0s.in)

## Building locally

To set up and work with this project locally, follow the steps below:

1. Fork, clone or download this project
1. [Install][] Hexo and [Hexo Server][hexo-server]
1. Install dependencies with `npm install`
1. Run the preview locally: `hexo server`
    1. Use `-p <port>` to run on a different port than default `4000`
1. To add the writeups:
    * Add content to `source/_posts/<category>/` in markdown
    * Or 'hexo new post <title>' to create with post_asset_folder
1. Generate your site (optional): `hexo generate`

Read more at [Hexo docs](https://hexo.io/docs/)

##  Features Implemented
* Author name to post layout
* Supports 2 Authors, and Author Webpage link
* Google-Analytics
* Post Encrypt
* PWA
* Local Search
* Slide HTML <alpha>
* Disqus Comment
* Local Search

## Ongoing Tasks
* Post breadcrumb
* Google CSE

## To-Do
* GDrive Sync
* AMP
* Custom Fields

## Tips:
* Always use `<!--more-->` to split the post with excerpt
* Use footnotes with [^1] 
    * More Syntax [here](https://github.com/LouisBarranqueiro/hexo-footnotes)
* Slide HTML in  front-matter, (use any one of the folowing) 
    * slidehtml: true
    * slidehtml:
        titleMerge: true
        verticalSeparator: \n--\n
    * slidehtml: 
        titleMerge: true
* Never Encrypt your blog
* Use author2 and author2-url for 2nd author

## Sample Front-Matter
```front-matter
---
title: Pretty

Complete this!!!
```
